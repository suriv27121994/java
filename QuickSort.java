

import java.util.Random;

public class QuickSort  {
    private int[] numbers;
    private int number;

    public void sort(int[] values) {
        // check for empty or null array
        if (values ==null || values.length==0){
            return;
        }
        this.numbers = values;
        number = values.length;
        quicksort(0, number - 1);
		
		for(int number : numbers)
			System.out.print(number +",");
    }

    private void quicksort(int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = numbers[low + (high-low)/2];

        // Divide into two lists
        while (i <= j) {
            // If the current value from the left list is smaller than the pivot
            // element then get the next element from the left list
            while (numbers[i] < pivot) {
                i++;
            }
            // If the current value from the right list is larger than the pivot
            // element then get the next element from the right list
            while (numbers[j] > pivot) {
                j--;
            }

            // If we have found a value in the left list which is larger than
            // the pivot element and if we have found a value in the right list
            // which is smaller than the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
    }

    private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
	
	public static void main(String [] args)
	{
		
		int[] random_value = new int[15000000];
        Random random = new Random();

        System.out.println("Before sorting: ");
        for(int i=0;i<random_value.length;i++)
        {
            random_value[i]=random.nextInt(random_value.length);
            System.out.print(random_value[i]+", ");
        }
		System.out.println("\n\n");
		
		//record time
		long maxMemory=0;
        long begin = System.currentTimeMillis();
        Runtime runtime = Runtime.getRuntime();
        System.gc();
        
        long freememory1 = runtime.freeMemory();
		System.out.println("Maximum memory (bytes): " + (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));
		
		//sorting data
		QuickSort quickSort = new QuickSort();
		quickSort.sort(random_value);
		
		//record end time
        long end = System.currentTimeMillis();
        long freememory2 = runtime.freeMemory();
        
        //print time and memory
        System.out.println("\nTime taken: "+(end-begin));
        System.out.println("Memory: "+(freememory2-freememory1));
		
		long totalMemory = Runtime.getRuntime().totalMemory();
		System.out.println("Total memory (bytes): " +totalMemory);
	}
}
